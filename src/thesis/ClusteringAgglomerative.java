package thesis;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class ClusteringAgglomerative {
	
	private int numberOfFace;
	private File[] faces; 
	private int representative[] ;
	
	private Map<String, String> personMap;
	
	private Map<Integer, ArrayList<String>> clusterMap;
	
	public ClusteringAgglomerative() {
		File folder = new File(Path.DETECTED_FACE);
		faces = folder.listFiles();
		numberOfFace = faces.length;
		
		initRepresentative();
		initPersonName();
		
		run();
		calculateFinalCluster();
		saveCluster();
		
		
	}
	
	private void initRepresentative() {
		representative = new int[numberOfFace];
		for (int i=0; i<numberOfFace ;i++)
			representative[i] = i;
	}
	
	private void initPersonName() {
		//map name of person for each file
		personMap = new HashMap<String, String>();
		for (int i = 0; i < faces.length; i++) {
			personMap.put(faces[i].getName(), faces[i].getName().substring(0, faces[i].getName().indexOf(".")));
		}
	}
	
	private boolean isSamePerson(String person1, String person2) {
		return personMap.get(person1).equals(personMap.get(person2));
	}
	
	private int findRepresentative(int n) {
		if (representative[n]==n) {
			return n;
		}
		//n er representative ki n?
		//jehetu ager na
		//n er new representative hobe, (n er representative) er representative
		representative[n]= findRepresentative(representative[n]);
		return representative[n];
	}
	
	private void union(int a, int b) {
		int u = findRepresentative(a);
		int v = findRepresentative(b);
		//System.out.println("Union: "+a+"("+findRepresentative(a)+") "+b+"("+findRepresentative(b)+")");
		if(u!=v){
			representative[v] =u ;
		}
		//System.out.println("after: "+a+"("+findRepresentative(a)+") "+b+"("+findRepresentative(b)+")");
	}
	
	public void run() {
		ArrayList<ClusterInfo> allCluster = new ArrayList<ClusterInfo>();
		
		//save every possible pair of image
		
		new LoadDifference();
		
		for (int i = 0; i < faces.length; i++) {
			for (int j = i+1; j < faces.length; j++) {
				Difference difference = new Difference(faces[i], faces[j], 
						FileType.DETECTED_FACE_IMAGE, FileType.SAVED_DIFFERENCE);
				ClusterInfo clusterInfo = 
						new ClusterInfo(i, j, difference.getTotalDistance(),
								difference.getTotalEucledianDistance());
				allCluster.add(clusterInfo);				
			}
			System.out.println("Making every possible pair: iteration: "+i);
		}
		
		//sort all cluster pair, the rule of agglomerative clustering is to take 
		//the pair where the difference is minimum
		Collections.sort(allCluster);
				
		//for all pair
		int counter= 1;
		for (ClusterInfo clusterInfo : allCluster) {
			String name1 = faces[clusterInfo.getIndex1()].getName();
			String name2 = faces[clusterInfo.getIndex2()].getName();
			boolean equal = isSamePerson(name1, name2);
			System.out.println("["+counter++ +"] " +clusterInfo+"   "+(equal?"1":"0")+"   "+name1 +" "+name2);
			
			if (clusterInfo.getTotalDistance()<Threshold.LOW) {
				//System.out.println("same person");
				union(clusterInfo.getIndex1(), clusterInfo.getIndex2());
			}
		}
		
	}
	
	public void calculateFinalCluster() {
		
		//path compression
		//changing the representative
		//if not already minimized
		for (int i = 0; i < representative.length; i++) {
			if (representative[i]!=i) {
				representative[i] = findRepresentative(i);
			}
		}
		
		//make final cluster
		clusterMap = new HashMap<Integer, ArrayList<String>>();
		for (int i = 0; i < representative.length; i++) {
			int representativeValue = representative[i];
			
			if (clusterMap.containsKey(representativeValue)) {
				ArrayList<String> arrayList = clusterMap.get(representativeValue);
				arrayList.add(faces[i].getName().toString());
				clusterMap.put(representativeValue, arrayList);
			}
			else {
				ArrayList<String> arrayList = new ArrayList<String>();
				arrayList.add(faces[i].getName().toString());
				clusterMap.put(representativeValue, arrayList);
			}
		}
	}
	
	public Map<Integer, ArrayList<String>> getClusterMap() {
		return clusterMap;
	}
	
	public ArrayList< ArrayList<String>> getArrayListOfCluster() {
		ArrayList<ArrayList<String>> ret = new ArrayList<ArrayList<String>>();
		for (Integer integer : clusterMap.keySet()) {
			ret.add(clusterMap.get(integer));
		}
		return ret;
	}
	
	public void saveCluster() {
		File clusterOutputFile = new File(Path.INFO+"clusterInfo.txt");
		try {
			FileWriter fileWriter = new FileWriter(clusterOutputFile);
			int clusterNo=1;
			for (Integer integer : clusterMap.keySet()) {
				fileWriter.write("Cluster "+clusterNo++ +": "+System.lineSeparator());
				ArrayList<String> arrayList = clusterMap.get(integer);
				for (String string : arrayList) {
					fileWriter.write(string+System.lineSeparator());
				}
				fileWriter.write(System.lineSeparator());
			}
			fileWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
		
	public static void main(String args[]) {
		ClusteringAgglomerative clustering = new ClusteringAgglomerative();
		System.out.println(clustering.getArrayListOfCluster());
	}
	
}
