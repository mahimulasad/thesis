package thesis;

public class ClusterInfo implements Comparable<ClusterInfo>{
	
	private int index1;
	private int index2;
	private int totalDistance;
	private int totalEucledianDistance;
	private boolean samePerson;
	
	public ClusterInfo(int index1, int index2, int totalDistance, int totalEucledianDistance) {
		this.index1 = index1;
		this.index2 = index2;
		this.totalDistance = totalDistance;
		this.totalEucledianDistance = totalEucledianDistance;
	}

	public ClusterInfo(int index1, int index2, int totalDistance, int totalEucledianDistance, boolean samePerson) {
		this.index1 = index1;
		this.index2 = index2;
		this.totalDistance = totalDistance;
		this.totalEucledianDistance = totalEucledianDistance;
		this.samePerson = samePerson;
	}
	
	public String toString() {
		return String.format("%04d %04d %04d %06d", index1, index2, totalDistance, totalEucledianDistance);
	}

	@Override
	public int compareTo(ClusterInfo o) {
		if (this.totalDistance == o.totalDistance) {
			if (this.totalEucledianDistance == o.totalEucledianDistance) {
				if (this.index1 == o.index1) {
					return this.index2 - o.index2;
				}
				return this.index1 - o.index1;
			}
			return this.totalEucledianDistance - o.totalEucledianDistance;
		}
		return this.totalDistance-o.totalDistance;
	}
	
//	@Override
//	public int compareTo(ClusterSorterInfo o) {
//		if (this.totalEucledianDistance == o.totalEucledianDistance) {
//			if (this.totalDistance == o.totalDistance) {
//				if (this.index1 == o.index1) {
//					return this.index2 - o.index2;
//				}
//				return this.index1 - o.index1;
//			}
//			return this.totalDistance - o.totalDistance;
//		}
//		return this.totalEucledianDistance -o.totalEucledianDistance;
//	}

	public int getIndex1() {
		return index1;
	}

	public int getIndex2() {
		return index2;
	}

	public int getTotalDistance() {
		return totalDistance;
	}

	public int getTotalEucledianDistance() {
		return totalEucledianDistance;
	}
	
	public boolean isSamePerson(){
		return samePerson;
	}
}
