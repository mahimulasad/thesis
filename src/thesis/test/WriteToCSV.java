package thesis.test;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class WriteToCSV {

	public static void main(String[] args) throws IOException {
		File  file = new File("test.csv");
		FileWriter fileWriter = new FileWriter(file);
		fileWriter.write(1234 + ",");
		fileWriter.write(3456+",");
		fileWriter.write(System.lineSeparator());
		fileWriter.write(4567 + ",");
		fileWriter.write(6556+",");
		fileWriter.write(System.lineSeparator());
		fileWriter.close();
	}

}
