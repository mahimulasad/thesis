package thesis.test;

import java.awt.Color;
import java.awt.Graphics;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;


public class LBPtoTemplete {
	
	public static int templete[] = new int[256];
	
	public static void main	(String args[]) throws IOException {
		
		File lbpFile = new File("lbp.txt");
		Scanner cin = new Scanner(lbpFile);
		
		int row= cin.nextInt();
		int col= cin.nextInt();
		
		
		for (int i = 0; i < row; i++) {
			for (int j = 0; j < col; j++) {
				int value = cin.nextInt();
				templete[value]++;
			}
		}
		
		File templeteFile = new File("templete.txt");
		FileWriter fw = new FileWriter(templeteFile);
		for (int i = 0; i < templete.length; i++) {
			fw.write(templete[i]+ " ");
		}
		fw.close();
		
		//System.out.println("test");
		JFrame frame = new JFrame();
		DrawTemplete drawTemplete = new DrawTemplete();
		frame.setTitle("Feature Templete");
		frame.setSize(820,400);
		frame.getContentPane().add(drawTemplete);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//System.out.println("test2");
	}

}


class DrawTemplete extends JComponent{
	
	public void paint(Graphics g){
		//System.out.println(getWidth()+" "+ getSize());
		int widthPerBit=3;
		int height = 300;
		g.setColor(Color.BLACK);
		g.drawRect(0+10, 10, 256*widthPerBit+10, height-10);
		g.setColor(Color.BLUE);
		for (int i = 0; i < LBPtoTemplete.templete.length; i++) {
			g.fillRect(i*widthPerBit+10, height-LBPtoTemplete.templete[i], widthPerBit, LBPtoTemplete.templete[i]);
		}
	}
}