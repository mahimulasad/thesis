package thesis.test;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.IntervalMarker;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.xy.IntervalXYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.Layer;
import org.jfree.ui.RectangleAnchor;
import org.jfree.ui.RefineryUtilities;
import org.jfree.ui.TextAnchor;

import thesis.Path;

/**
 * This demo shows a simple bar chart created using the {@link XYSeriesCollection} dataset.
 *
 */

public class VisualizeLBPfeatureVector extends ApplicationFrame{
	 /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
     * Creates a new demo instance.
     *
     * @param title  the frame title.
     */
    public VisualizeLBPfeatureVector(final String title) {
        super(title);
        JPanel mainPanel = new JPanel();
        File folder = new File(Path.DETECTED_FACE);
        mainPanel.setLayout(new GridLayout(folder.listFiles().length, 2));
        mainPanel.setSize(700, 500);
        for (File files : folder.listFiles()) {
			if (!files.isDirectory()) {
				ImageIcon image = new ImageIcon(files.getAbsolutePath().toString());
				mainPanel.add(new JLabel(image));
				String lbpFeatureFileName= Path.LBP_FEATURE_VECTOR+"lbp_feature_"+files.getName().toString()+".txt";
				IntervalXYDataset dataset = createDataset(lbpFeatureFileName);
		        JFreeChart chart = createChart(dataset);
		        final ChartPanel chartPanel = new ChartPanel(chart);
		        mainPanel.add(chartPanel);
		        chartPanel.setPreferredSize(new java.awt.Dimension(520, 270));
		        
			}
			
		}
        JScrollPane jScrollPane = new JScrollPane(mainPanel);
        add(jScrollPane);
        //setContentPane(this);
    }
    
    /**
     * Creates a sample dataset.
     * 
     * @return A sample dataset.
     */
    private IntervalXYDataset createDataset(String filename) {
        final XYSeries series = new XYSeries("LBP feature:"+filename);
        series.add(0,null);
        series.add(255,null);
        File file = new File(filename);
        try {
			Scanner cin = new Scanner(file);
			//input 1st 3 lines, that are not needed
			cin.nextLine();
			cin.nextLine();
			cin.nextLine();
			for (int i = 0; i < 256; i++) {
				int value = cin.nextInt();
				series.add(i,value);
			}
			cin.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
        final XYSeriesCollection dataset = new XYSeriesCollection(series);
        return dataset;
    }

    /**
     * Creates a sample chart.
     * 
     * @param dataset  the dataset.
     * 
     * @return A sample chart.
     */
    private JFreeChart createChart(IntervalXYDataset dataset) {
        final JFreeChart chart = ChartFactory.createXYBarChart(
            "LBP feature vector",
            "X", 
            false,
            "Y", 
            dataset,
            PlotOrientation.VERTICAL,
            true,
            true,
            false
        );
        XYPlot plot = (XYPlot) chart.getPlot();
        final IntervalMarker target = new IntervalMarker(400.0, 700.0);
        target.setLabel("Target Range");
        target.setLabelFont(new Font("SansSerif", Font.ITALIC, 11));
        target.setLabelAnchor(RectangleAnchor.LEFT);
        target.setLabelTextAnchor(TextAnchor.CENTER_LEFT);
        target.setPaint(new Color(222, 222, 255, 128));
        plot.addRangeMarker(target, Layer.BACKGROUND);
        return chart;    
    }
    
    // ****************************************************************************
    // * JFREECHART DEVELOPER GUIDE                                               *
    // * The JFreeChart Developer Guide, written by David Gilbert, is available   *
    // * to purchase from Object Refinery Limited:                                *
    // *                                                                          *
    // * http://www.object-refinery.com/jfreechart/guide.html                     *
    // *                                                                          *
    // * Sales are used to provide funding for the JFreeChart project - please    * 
    // * support us so that we can continue developing free software.             *
    // ****************************************************************************
    
    /**
     * Starting point for the demonstration application.
     *
     * @param args  ignored.
     */
    public static void main(final String[] args) {

        final VisualizeLBPfeatureVector demo = new VisualizeLBPfeatureVector("LBP feature vector");
        demo.pack();
        RefineryUtilities.centerFrameOnScreen(demo);
        demo.setVisible(true);

    }


}
