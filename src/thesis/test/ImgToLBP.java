package thesis.test;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;


public class ImgToLBP {

	public static final String PATH ="E:/Research/Thesis/Implementation/face detection/imageValue.txt";
	
	public static final int[] r = {-1, -1, -1, 0, 1, 1, 1, 0};
	public static final int[] c = {-1, 0, 1, 1, 1, 0, -1, -1};
	
	public static int row, col;
	public static int[][] img;
	public static int[][] lbp;
	public static void main(String[] args) throws IOException {

		File imgFile = new File(PATH);
		Scanner cin = new Scanner(imgFile);
		
		row = cin.nextInt();
		col = cin.nextInt();
		
		img =  new int[row][col];
		lbp = new int[row][col];
		
		for (int i = 0; i < row; i++) {
			for (int j = 0; j < col; j++) {
				img[i][j] = cin.nextInt();
			}
		}
		
		for (int i = 1; i < row-1; i++) {
			for (int j = 1; j < col-1; j++) {
				int thisBit= img[i][j];
//				System.out.println("bit["+i+"]["+j+"] = "+ img[i][j]);
				String binary="";
				for (int k=0; k<8; k++){
					int neighborBit = img[i+r[k]][j+c[k]];
//					System.out.print("->-> "+neighborBit );
					if (thisBit>neighborBit) {
						binary+= "0";
//						System.out.println(" (0)");
					}
					else {
						binary+="1";
//						System.out.println(" (1)");
					}
				}
//				System.out.println(binary);
				int decimal = Integer.parseInt(binary, 2);
//				System.out.println(decimal);
				lbp[i][j] = decimal;
			}
		}
		
		FileWriter fw = new FileWriter("lbp.txt");
		fw.write((row-2) +" "+ (col-2) + " \n");
		for (int i = 1; i < row-1; i++) {
			for (int j = 1; j < col-1; j++) {
				fw.write(String.format("%3d  ", lbp[i][j]));
			}
			fw.write("\n");
		}
		fw.close();
		
	}
	
	
}
