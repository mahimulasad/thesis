package thesis.test;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class CompareMatlabValue {

	public static int matlabValue[][] = new int[30][30];
	public static int programValue[][] = new int[30][30];
	
	public static void main(String[] args) {
		
		readFromMatlab();
		readFromJavaProgram();
		System.out.println(compareTwoArray()?"Same value":"Different Value");
		displayValue();
	}
	
	public static void readFromMatlab() {
		File file = new File("E:/Research/Thesis/Implementation/lbp_matlab.txt");
		//Matlab value are written in column by column
		try {
			Scanner cin = new Scanner(file);
			for (int i = 0; i < 30; i++) {
				for (int j = 0; j < 30; j++) {
					matlabValue[j][i] = cin.nextInt();
				}
			}
			cin.close();
		} catch (FileNotFoundException e) {
			System.exit(-1);
			e.printStackTrace();
		}
		
	}
	
	public static void readFromJavaProgram() {
		File file = new File("data/current/detected_face_lbp/lbp_gp1.jpg_0.png.txt");
		try {
			Scanner cin = new Scanner(file);
			cin.nextLine();
			cin.nextLine();
			cin.nextLine();
			cin.nextLine();
			for (int i = 0; i < programValue.length; i++) {
				for (int j = 0; j < programValue[i].length; j++) {
					programValue[i][j] = cin.nextInt();
				}
			}
			cin.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public static boolean compareTwoArray() {
		for (int i = 0; i < matlabValue.length; i++) {
			for (int j = 0; j < matlabValue[i].length; j++) {
				if (matlabValue[i][j]!=programValue[i][j]) {
					return false;
				}
			}
		}
		return true;
	}
	
	public static void displayValue() {
		for (int i = 0; i < matlabValue.length; i++) {
			for (int j = 0; j < matlabValue[i].length; j++) {
				System.out.println((matlabValue[i][j]==programValue[i][j])+" "+i+" "+j+" "+matlabValue[i][j]+" "+programValue[i][j]);
			}
		}
	}

}
