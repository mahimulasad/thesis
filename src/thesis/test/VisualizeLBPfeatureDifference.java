package thesis.test;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.BoundedRangeModel;
import javax.swing.ImageIcon;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.KeyStroke;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.IntervalMarker;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.xy.IntervalXYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.Layer;
import org.jfree.ui.RectangleAnchor;
import org.jfree.ui.RefineryUtilities;
import org.jfree.ui.TextAnchor;

import thesis.Difference;
import thesis.FileType;
import thesis.Path;

/**
 * This demo shows a simple bar chart created using the {@link XYSeriesCollection} dataset.
 *
 */

public class VisualizeLBPfeatureDifference extends ApplicationFrame{
	 /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
     * Creates a new demo instance.
     *
     * @param title  the frame title.
     */
    public VisualizeLBPfeatureDifference(final String title) {
        super(title);
        JPanel mainPanel = new JPanel();
        File folder = new File(Path.DETECTED_FACE);
        File fileList[] = folder.listFiles();
        mainPanel.setLayout(new GridLayout(fileList.length*fileList.length, 2));
        mainPanel.setSize(700, 500);
        
        for (int i=0; i<fileList.length; i++) {
        	for (int j = 0; j < fileList.length; j++) {
				System.out.println(i+" "+j);
        		if (!fileList[i].isDirectory() && !fileList[j].isDirectory()) {
        			ImageIcon image1 = new ImageIcon(fileList[i].getAbsolutePath().toString());
        			ImageIcon image2 = new ImageIcon(fileList[j].getAbsolutePath().toString());
        			JPanel infoDisplayPanel = new JPanel();
        	        infoDisplayPanel.setLayout(new FlowLayout());
        			infoDisplayPanel.add(new JLabel(image1));
        			infoDisplayPanel.add(new JLabel(image2));
        			String lbpFeatureFileName1= Path.LBP_FEATURE_VECTOR+"lbp_feature_"+fileList[i].getName().toString()+".txt";
        			String lbpFeatureFileName2= Path.LBP_FEATURE_VECTOR+"lbp_feature_"+fileList[j].getName().toString()+".txt";
        			
        			Difference difference = new Difference(lbpFeatureFileName1, lbpFeatureFileName2, FileType.LBP_FEATURE_VECTOR);
        			infoDisplayPanel.add(new JLabel("Total Distance: "+difference.getTotalDistance()));
        			infoDisplayPanel.add(new JLabel("Total Eucledian Distance: "+difference.getTotalEucledianDistance()));
        			IntervalXYDataset dataset = createDataset(lbpFeatureFileName1, lbpFeatureFileName2, difference.getDistanceArray());
        			JFreeChart chart = createChart(dataset);
        			final ChartPanel chartPanel = new ChartPanel(chart);
        			mainPanel.add(infoDisplayPanel);
        			mainPanel.add(chartPanel);
        			chartPanel.setPreferredSize(new java.awt.Dimension(520, 270));
        			
        		}
			}
			
		}
        JScrollPane jScrollPane = new JScrollPane(mainPanel);
        
        InputMap inputMap = jScrollPane.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
        ActionMap actionMap = jScrollPane.getActionMap();
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_UP,0), "UP");
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_DOWN,0), "DOWN");
        
        int scrollableIncrement =50;
        actionMap.put("UP", new UpDownAction("UP", jScrollPane.getVerticalScrollBar().getModel(), scrollableIncrement));
        actionMap.put("DOWN", new UpDownAction("DOWN", jScrollPane.getVerticalScrollBar().getModel(), scrollableIncrement));
        
        add(jScrollPane);
        //setContentPane(this);
    }
    
    private class UpDownAction extends AbstractAction {
    	
    	/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		private BoundedRangeModel verticalScrollBarModel;
    	private int scrollableIncrement;
    	
    	public UpDownAction(String name, BoundedRangeModel verticalScrollBarModel, int scrollableIncrement) {
    		super(name);
    		this.verticalScrollBarModel = verticalScrollBarModel;
    		this.scrollableIncrement = scrollableIncrement;
    	}

		@Override
		public void actionPerformed(ActionEvent e) {
			String name = getValue(AbstractAction.NAME).toString();
			int value = verticalScrollBarModel.getValue();
			if (name.equals("UP")) {
				value -= scrollableIncrement;
				verticalScrollBarModel.setValue(value);
			}
			else if (name.equals("DOWN")) {
				value += scrollableIncrement;
				verticalScrollBarModel.setValue(value);
			}
		}
    	
    }
    
    /**
     * Creates a sample dataset.
     * 
     * @return A sample dataset.
     */
    private IntervalXYDataset createDataset(String filename1, String filename2, int distance[]) {
        final XYSeries series = new XYSeries("LBP feature differnce:\n"+filename1+"\n"+filename2);
        series.add(0,null);
        series.add(255,null);
        for (int i = 0; i < distance.length; i++) {
        	series.add(i, distance[i]);
		}

        final XYSeriesCollection dataset = new XYSeriesCollection(series);
        return dataset;
    }

    /**
     * Creates a sample chart.
     * 
     * @param dataset  the dataset.
     * 
     * @return A sample chart.
     */
    private JFreeChart createChart(IntervalXYDataset dataset) {
        final JFreeChart chart = ChartFactory.createXYBarChart(
            "LBP feature vector",
            "X", 
            false,
            "Y", 
            dataset,
            PlotOrientation.VERTICAL,
            true,
            true,
            false
        );
        XYPlot plot = (XYPlot) chart.getPlot();
        final IntervalMarker target = new IntervalMarker(400.0, 700.0);
        target.setLabel("Target Range");
        target.setLabelFont(new Font("SansSerif", Font.ITALIC, 11));
        target.setLabelAnchor(RectangleAnchor.LEFT);
        target.setLabelTextAnchor(TextAnchor.CENTER_LEFT);
        target.setPaint(new Color(222, 222, 255, 128));
        plot.addRangeMarker(target, Layer.BACKGROUND);
        return chart;    
    }
    
    // ****************************************************************************
    // * JFREECHART DEVELOPER GUIDE                                               *
    // * The JFreeChart Developer Guide, written by David Gilbert, is available   *
    // * to purchase from Object Refinery Limited:                                *
    // *                                                                          *
    // * http://www.object-refinery.com/jfreechart/guide.html                     *
    // *                                                                          *
    // * Sales are used to provide funding for the JFreeChart project - please    * 
    // * support us so that we can continue developing free software.             *
    // ****************************************************************************
    
    /**
     * Starting point for the demonstration application.
     *
     * @param args  ignored.
     */
    public static void main(final String[] args) {

        final VisualizeLBPfeatureDifference demo = new VisualizeLBPfeatureDifference("LBP feature vector");
        demo.pack();
        RefineryUtilities.centerFrameOnScreen(demo);
        demo.setVisible(true);

    }


}
