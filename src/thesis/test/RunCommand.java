package thesis.test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class RunCommand {

	public static void main(String[] args) throws IOException {
		String command = "test.exe";
		StringBuffer output= new StringBuffer("");
		Process p;
		try {
			p = Runtime.getRuntime().exec(command);
			p.waitFor();
			BufferedReader reader = 
                           new BufferedReader(new InputStreamReader(p.getInputStream()));

			String line = "";			
			while ((line = reader.readLine())!= null) {
//				output.append(line + "\n");
				System.out.println(line);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	//	System.out.println(output.toString());

	}

}
