package thesis;

public class Main1 {

	public static void main(String[] args) {
		
		//Delete old files
		System.out.println("Deleting old files");
		DeleteOldFiles deleteOldFiles = new DeleteOldFiles();
		deleteOldFiles.run();
		System.out.println();
		
		//create list of input image and save it to data/current/info/InputImageList.txt 
		System.out.println("Creating file list of input image...");
		InputImageList inputImageList = new InputImageList();
		inputImageList.create();
		System.out.println();
		
		System.out.println("Now run face detection process....");
		
	}

}
