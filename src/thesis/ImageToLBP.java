/*
 * This program generates lbp value
 * and lbp feature vector/template from image value
 */
package thesis;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class ImageToLBP {
	
	private final int[] r = {-1, -1, -1, 0, 1, 1, 1, 0};
	private final int[] c = {-1, 0, 1, 1, 1, 0, -1, -1};
	
	private final String DetectedFaceValuePath= "data/current/detected_face_value/";
	private final String DetectedFaceLBPpath = "data/current/detected_face_lbp/";
	private final String LBPfeatureVector = "data/current/lbp_feature_vector/";
	
	public ImageToLBP() {
	}
	
	public void run() {
		File folder= new File(DetectedFaceValuePath);
		for (File files : folder.listFiles()) {
			if (!files.isDirectory()) {
				ImageToLBPprocess(files, files.getName());
			}				
		}
	}
	
	
	private void ImageToLBPprocess(File file, String filename) {
		int row, col;
		int[][] img;
		int[][] lbp;
		
		try {
			Scanner cin = new Scanner(file);
			String originalImageName = cin.nextLine();
			String croppedImageName = cin.nextLine();
			cin.next();//input the string "size"
			
			row = cin.nextInt();
			col = cin.nextInt();
			
			img =  new int[row][col];
			lbp = new int[row][col];
			
			//image value input
			for (int i = 0; i < row; i++) {
				for (int j = 0; j < col; j++) {
					img[i][j] = cin.nextInt();
				}
			}
			
			cin.close();
			
			//generating lbp value for each cell
			for (int i = 1; i < row-1; i++) {
				for (int j = 1; j < col-1; j++) {
					int thisBit= img[i][j];
//					System.out.println("bit["+i+"]["+j+"] = "+ img[i][j]);
					String binary="";
					for (int k=0; k<8; k++){
						int neighborBit = img[i+r[k]][j+c[k]];
//						System.out.print("->-> "+neighborBit );
						if (thisBit>=neighborBit) {
							binary+= "0";
//							System.out.println(" (0)");
						}
						else {
							binary+="1";
//							System.out.println(" (1)");
						}
					}
//					System.out.println(binary);
					int decimal = Integer.parseInt(binary, 2);
//					System.out.println(decimal);
					lbp[i][j] = decimal;
				}
			}
			
			//save the lbp value into file
			File outputFile = new File(DetectedFaceLBPpath+"lbp_"+filename);
			if (!outputFile.exists()) {
				outputFile.createNewFile();
			}
			BufferedWriter fw = new BufferedWriter(new FileWriter(outputFile));
			fw.write(originalImageName+"\r\n");
			fw.write(croppedImageName+"\r\n");
			fw.write("row: "+(row-2)+"\r\n");
			fw.write("col: "+(col-2)+"\r\n");
			for (int i = 1; i < row-1; i++) {
				for (int j = 1; j < col-1; j++) {
					fw.write(String.format("%3d  ", lbp[i][j]));
				}
				fw.write("\r\n");
			}
			fw.close();
			
			//generate lbp feature vector/template 
			int featureVector[] = new int[256];
			for (int i = 0; i < lbp.length; i++) {
				for (int j = 0; j < lbp[i].length; j++) {
					featureVector[ lbp[i][j] ]++;
				}
			}
			
			//save the lbp feature vector into file
			outputFile = new File(LBPfeatureVector+"lbp_feature_"+filename);
			if (!outputFile.exists()) {
				outputFile.createNewFile();
			}
			
			fw = new BufferedWriter(new FileWriter(outputFile));
			fw.write(originalImageName+"\r\n");
			fw.write(croppedImageName+"\r\n");
			fw.write("256 dimension feature vector:\r\n");
			for (int i = 0; i < featureVector.length; i++) {
				fw.write(featureVector[i]+ " ");
			}
			fw.write("\r\n");
			fw.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
	}
	
	public static void main(String[] args) {
		new ImageToLBP().run();
	}

}
