package thesis;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class LoadDifference {
	
	public static Map<String, Integer> indexMap;
	public static int[][] lbpDistance;
	public static int[][] lbpEucledianDistance;
	
	public LoadDifference() {
		System.out.println("Loading all pair cluster information...");
		//load name into map
		File nameFile = new File(Path.DIFFERENCE+"name.txt");
		try {
			int counter = 0;
			indexMap = new HashMap<String, Integer>();
			Scanner cin = new Scanner(nameFile);
			while (cin.hasNext()) {
				String name = cin.next();
				indexMap.put(name, counter);
				counter++;
			}
			cin.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		//load lbp distance and lbp eucledian distance
		int size = indexMap.size();
		lbpDistance = new int[size][size];
		lbpEucledianDistance = new int[size][size];
		
		File lbpDistanceFile = new File(Path.DIFFERENCE+"lbp_distance.txt");
		File lbpEucledianDistanceFile = 
				new File(Path.DIFFERENCE+"lbp_eucledian_distance.txt");
		try {
			Scanner cinLbpDistance = new Scanner(lbpDistanceFile);
			Scanner cinLbpEucledianDistance = new Scanner(lbpEucledianDistanceFile);
			for (int i = 0; i < size; i++) {
				for (int j = i+1; j < size; j++) {
					lbpDistance[i][j] = cinLbpDistance.nextInt();
					lbpDistance[j][i] = lbpDistance[i][j];
					lbpEucledianDistance[i][j] = cinLbpEucledianDistance.nextInt();
					lbpEucledianDistance[j][i] = lbpEucledianDistance[i][j];
				}
			}
			cinLbpDistance.close();
			cinLbpEucledianDistance.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public static int getLbpDistance(File file1, File file2) {
		return lbpDistance[indexMap.get(file1.getName())][indexMap.get(file2.getName())];
	}
	
	public static int getLbpEucledianDistance(File file1, File file2) {
		return lbpEucledianDistance[indexMap.get(file1.getName())][indexMap.get(file2.getName())];
	}

	public static void main(String[] args) {
		new LoadDifference();
		File file1 = new File("mkosto.10.jpg_0.png");
		File file2 = new File("mizli.1.jpg_0.png");
		System.out.println(LoadDifference.getLbpDistance(file1, file2));
		System.out.println(LoadDifference.getLbpEucledianDistance(file1, file2));
	}

}
