package thesis;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import thesis.FileType;
import thesis.Path;

public class Difference {
	
	private String lbpFeatureFileName1 = null;
	private String lbpFeatureFileName2 = null;
	private int[] lbpFeatureVector1;
	private int[] lbpFeatureVector2;
	private int distanceArray[];
	private int eucledianDistanceArray[];
	private int totalDistance ;
	private int totalEucledianDistance;
	
	
	public Difference(String filename1, String filename2, int mode) {
		if (mode== FileType.DETECTED_FACE_IMAGE) {
			lbpFeatureFileName1 = Path.LBP_FEATURE_VECTOR+"lbp_feature_"+filename1+".txt";
			lbpFeatureFileName2= Path.LBP_FEATURE_VECTOR+"lbp_feature_"+filename2+".txt";
			
		}
		else if (mode== FileType.LBP_FEATURE_VECTOR) {
			this.lbpFeatureFileName1 = filename1;
			this.lbpFeatureFileName2 = filename2;
		}
		
		lbpFeatureVector1 = getLBPfeatureVector(lbpFeatureFileName1);
		lbpFeatureVector2 = getLBPfeatureVector(lbpFeatureFileName2);
		run();
	}
	
	public Difference(File file1, File file2, int mode) {
		if (mode== FileType.DETECTED_FACE_IMAGE) {
			lbpFeatureFileName1= Path.LBP_FEATURE_VECTOR+"lbp_feature_"+file1.getName().toString()+".txt";
			lbpFeatureFileName2= Path.LBP_FEATURE_VECTOR+"lbp_feature_"+file2.getName().toString()+".txt";
			
		}
		else if (mode== FileType.LBP_FEATURE_VECTOR) {
			this.lbpFeatureFileName1 = file1.getName().toString();
			this.lbpFeatureFileName2 = file2.getName().toString();
		}
		
		lbpFeatureVector1 = getLBPfeatureVector(lbpFeatureFileName1);
		lbpFeatureVector2 = getLBPfeatureVector(lbpFeatureFileName2);
		run();
	}
	
	public Difference(File file1, File file2, int mode, int mode2) {
		if (mode== FileType.DETECTED_FACE_IMAGE && mode2 == FileType.SAVED_DIFFERENCE) {
			this.totalDistance = LoadDifference.getLbpDistance(file1, file2);
			this.totalEucledianDistance = LoadDifference.getLbpEucledianDistance(file1, file2);
		}
		else {
			System.err.println("Wrong mode or mode2 in Difference.java constructor");
		}
		
	}
	
	public Difference(int[] lbpFeatureVector1, int[] lbpFeatureVector2){
		this.lbpFeatureVector1 = lbpFeatureVector1;
		this.lbpFeatureVector2 = lbpFeatureVector2;
		run();
	}
	
	public void run(){
		calculateDistanceArray();
		calculateEucledianDistanceArray();
		calculateTotalDistance();
		calcualteTotalEucledianDistance();	
	}
	
	public int[] getLBPfeatureVector(String filename) {
    	File file = new File(filename);
    	int A[] = new int[256];
    	try {
			Scanner cin = new Scanner(file);
			//input 1st 3 lines, that are not needed
			cin.nextLine();
			cin.nextLine();
			cin.nextLine();
			for (int i = 0; i < 256; i++) {
				A[i] = cin.nextInt();
			}
			cin.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
    	return A;
	}
    
    private void calculateDistanceArray() {
        distanceArray = new int[256];
    	for (int i = 0; i < lbpFeatureVector1.length; i++) {
        	distanceArray[i]= Math.abs(lbpFeatureVector1[i]-lbpFeatureVector2[i]);
		}
	}
    
    private void calculateTotalDistance() {
		totalDistance = 0;
		for (int i = 0; i < distanceArray.length; i++) {
			totalDistance+= distanceArray[i];
		}
	}
    
    private void calculateEucledianDistanceArray() {
        eucledianDistanceArray = new int[256];
    	for (int i = 0; i < lbpFeatureVector1.length; i++) {
        	eucledianDistanceArray[i]= square(lbpFeatureVector1[i]-lbpFeatureVector2[i]);
		}
	}
    
    private void calcualteTotalEucledianDistance() {
		totalEucledianDistance = 0;
		for (int i = 0; i < eucledianDistanceArray.length; i++) {
			totalEucledianDistance += eucledianDistanceArray[i];
		}
	}
    
    private int square(int a){
    	return a*a;
    }
    
    public int[] getLbpFeatureVector1() {
		return lbpFeatureVector1;
	}

	public int[] getLbpFeatureVector2() {
		return lbpFeatureVector2;
	}

	public int[] getDistanceArray() {
		return distanceArray;
	}

	public int[] getEucledianDistanceArray() {
		return eucledianDistanceArray;
	}

	public int getTotalDistance() {
		return totalDistance;
	}

	public int getTotalEucledianDistance() {
		return totalEucledianDistance;
	}
}
