package thesis;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.BorderFactory;
import javax.swing.BoundedRangeModel;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.KeyStroke;
import javax.swing.ScrollPaneConstants;

public class VisualizeClusteringProcess extends JFrame {
	
	private static final long serialVersionUID = 1L;
	private JPanel mainPanel;
	private int numberOfFace;
	private File[] faces; 
	private int representative[] ;
	private Map<Integer, ArrayList<Integer>> clusterMap;
	
	private final String UP = "UP";
	private final String DOWN = "DOWN";
	private final String LEFT = "LEFT";
	private final String RIGHT = "RIGHT";
	
	public VisualizeClusteringProcess() {
		super();
		this.setTitle("Face Clustering Process - Step by Step");
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(1280, 600);
		this.setLocation(50, 100);
		mainPanel = new JPanel();
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
		
		File folder = new File(Path.DETECTED_FACE);
		faces = folder.listFiles();
		numberOfFace = faces.length;
		
		clusteringProcess();
		
		//finally added everything
		JScrollPane mainScrollPane = new JScrollPane(mainPanel);
		mainScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		mainScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		
        InputMap inputMap = mainScrollPane.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
        ActionMap actionMap = mainScrollPane.getActionMap();
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_UP,0), UP);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_DOWN,0), DOWN);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, 0), LEFT);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, 0), RIGHT);
        
        int scrollableIncrement =50;
        actionMap.put(UP, new UpDownAction(UP, mainScrollPane.getVerticalScrollBar().getModel(), scrollableIncrement));
        actionMap.put(DOWN, new UpDownAction(DOWN, mainScrollPane.getVerticalScrollBar().getModel(), scrollableIncrement));
        actionMap.put(LEFT, new LeftRightAction(LEFT, mainScrollPane.getHorizontalScrollBar().getModel(), scrollableIncrement));
        actionMap.put(RIGHT, new LeftRightAction(RIGHT, mainScrollPane.getHorizontalScrollBar().getModel(), scrollableIncrement));
        
		add(mainScrollPane);
		
	}

	public void clusteringProcess() {
		
		initRepresentative();
		
		JLabel initialStageLabel = new JLabel("Initial Stage");
		initialStageLabel.setFont(new Font(initialStageLabel.getFont().getName(), Font.BOLD, 20));
		mainPanel.add(initialStageLabel);
		calculateFinalCluster();
		displayAllImage(-1,-1);
		
		ArrayList<ClusterInfo> clusterSorterInfoArrayList = new ArrayList<ClusterInfo>();
		new LoadDifference();
		// save every possible pair of image
		for (int i = 0; i < faces.length; i++) {
			for (int j = i+1; j < faces.length; j++) {
				if (!faces[i].isDirectory() && !faces[j].isDirectory()) {
					Difference difference = new Difference(faces[i], faces[j], 
							FileType.DETECTED_FACE_IMAGE, FileType.SAVED_DIFFERENCE);
					ClusterInfo clusterSorterInfo = new ClusterInfo(i, j, difference.getTotalDistance(),
							difference.getTotalEucledianDistance());
					clusterSorterInfoArrayList.add(clusterSorterInfo);
				}
			}
		}
		// sort them in specific order
		Collections.sort(clusterSorterInfoArrayList);
		
		int stepNo =1;
		// select few of them which meet the condition
		for (ClusterInfo clusterSorterInfo : clusterSorterInfoArrayList) {
			// System.out.println(clusterSorterInfo);
			JLabel stepInfo = new JLabel("Step: "+stepNo++ + " index: "+clusterSorterInfo.getIndex1()+" & " 
					+clusterSorterInfo.getIndex2() + " Differece: "+clusterSorterInfo.getTotalDistance() 
					+" (Normal) & "+clusterSorterInfo.getTotalEucledianDistance()+" (Eucledian)");
			stepInfo.setFont(new Font(initialStageLabel.getFont().getName(), Font.BOLD, 18));
			mainPanel.add(stepInfo);
			displayAllImage(clusterSorterInfo.getIndex1(), clusterSorterInfo.getIndex2());
			union(clusterSorterInfo.getIndex1(), clusterSorterInfo.getIndex2());
			calculateFinalCluster();
			displayAllImage(-1, -1);
		}
	}
	
	private void displayAllImage(int highlight1, int highlight2) {
		JPanel allFacePanel = new JPanel();
		allFacePanel.setLayout(new FlowLayout());
		for (Integer clusterNo : clusterMap.keySet()) {
			//System.out.println("test" + clusterNo);
			JPanel clusterPanel = new JPanel();
			clusterPanel.setLayout(new FlowLayout());
			ArrayList<Integer> clusterArrayList = clusterMap.get(clusterNo);
			for (Integer imageNo : clusterArrayList) {
				ImageIcon image = new ImageIcon(faces[imageNo].getAbsolutePath());
				JLabel imageLabel = new JLabel(image);
				if (imageNo==highlight1 || imageNo==highlight2) {
					imageLabel.setBorder(BorderFactory.createMatteBorder(3, 3, 3, 3, Color.RED));					
				}
				else {
					imageLabel.setBorder(BorderFactory.createMatteBorder(3, 3, 3, 3, Color.BLACK));
				}
				clusterPanel.add(imageLabel);
			}
			clusterPanel.setBorder(BorderFactory.createMatteBorder(5, 5, 5, 5, Color.BLUE));
			allFacePanel.add(clusterPanel);
		}
		mainPanel.add(allFacePanel);
	}
	
	
	private void initRepresentative() {
		representative = new int[numberOfFace];
		for (int i=0; i<numberOfFace ;i++)
			representative[i] = i;
	}
	
	private int findRepresentative(int n) {
		if (representative[n]==n) {
			return n;
		}
		representative[n]= findRepresentative(representative[n]);
		return representative[n];
	}
	
	private void union(int a, int b) {
		int u = findRepresentative(a);
		int v = findRepresentative(b);
		if(u!=v){
			representative[v] =u ;
		}
	}
	
	
	public void calculateFinalCluster() {
		//path compression
		//changing the representative
		for (int i = 0; i < representative.length; i++) {
			if (representative[i]!=i) {
				representative[i] = findRepresentative(i);
			}
		}
		
		//make final cluster
		clusterMap = new HashMap<Integer, ArrayList<Integer>>();
		for (int i = 0; i < representative.length; i++) {
			int representativeValue = representative[i];
			
			if (clusterMap.containsKey(representativeValue)) {
				ArrayList<Integer> arrayList = clusterMap.get(representativeValue);
				arrayList.add(i);
				clusterMap.put(representativeValue, arrayList);
			}
			else {
				ArrayList<Integer> arrayList = new ArrayList<Integer>();
				arrayList.add(i);
				clusterMap.put(representativeValue, arrayList);
			}
		}
	}
	
	public Map<Integer, ArrayList<Integer>> getClusterMap() {
		return clusterMap;
	}
	
	public ArrayList< ArrayList<Integer>> getArrayListOfCluster() {
		ArrayList<ArrayList<Integer>> ret = new ArrayList<ArrayList<Integer>>();
		for (Integer integer : clusterMap.keySet()) {
			ret.add(clusterMap.get(integer));
		}
		return ret;
	}

    private class UpDownAction extends AbstractAction {
    	
		private static final long serialVersionUID = 1L;
		private BoundedRangeModel verticalScrollBarModel;
    	private int scrollableIncrement;
    	
    	public UpDownAction(String name, BoundedRangeModel verticalScrollBarModel, int scrollableIncrement) {
    		super(name);
    		this.verticalScrollBarModel = verticalScrollBarModel;
    		this.scrollableIncrement = scrollableIncrement;
    	}

		@Override
		public void actionPerformed(ActionEvent e) {
			String name = getValue(AbstractAction.NAME).toString();
			int value = verticalScrollBarModel.getValue();
			if (name.equals(UP)) {
				value -= scrollableIncrement;
				verticalScrollBarModel.setValue(value);
			}
			else if (name.equals(DOWN)) {
				value += scrollableIncrement;
				verticalScrollBarModel.setValue(value);
			}
		}
    	
    }
    
    private class LeftRightAction extends AbstractAction {
    	
		private static final long serialVersionUID = 1L;
		private BoundedRangeModel horizontalScrollBarModel;
    	private int scrollableIncrement;
    	
    	public LeftRightAction(String name, BoundedRangeModel horizontalScrollBarModel, int scrollableIncrement) {
    		super(name);
    		this.horizontalScrollBarModel = horizontalScrollBarModel;
    		this.scrollableIncrement = scrollableIncrement;
    	}

		@Override
		public void actionPerformed(ActionEvent e) {
			String name = getValue(AbstractAction.NAME).toString();
			int value = horizontalScrollBarModel.getValue();
			if (name.equals(LEFT)) {
				value -= scrollableIncrement;
				horizontalScrollBarModel.setValue(value);
			}
			else if (name.equals(RIGHT)) {
				value += scrollableIncrement;
				horizontalScrollBarModel.setValue(value);
			}
		}
    	
    }


	
	public static void main(String[] args) {
		
		new VisualizeClusteringProcess();
		
	}

}
