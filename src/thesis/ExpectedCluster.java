package thesis;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ExpectedCluster {

	File folder;
	File[] faces;
	Map<String, ArrayList<String>> map;
	
	public ExpectedCluster() {
		folder = new File(Path.DETECTED_FACE);
		faces = folder.listFiles();
		map = new HashMap<String, ArrayList<String>>();
		for (File file : faces) {
			String name = file.getName().substring(0,file.getName().indexOf("."));
			ArrayList<String> temp;
			if (map.containsKey(name)) {
				temp = map.get(name);
			}
			else {
				temp = new ArrayList<String>();
			}
			temp.add(file.getName());
			map.put(name, temp);
		}
		
		//write output in file
		File expectedClusterFile = new File(Path.INFO+"expectedCluster.txt");
		try {
			FileWriter fileWriter = new FileWriter(expectedClusterFile);
			int clusterNo=1;
			for (String name : map.keySet()) {
				fileWriter.write("Cluster "+clusterNo++ +": "+System.lineSeparator());
				ArrayList<String> arrayList = map.get(name);
				for (String string : arrayList) {
					fileWriter.write(string+System.lineSeparator());
				}
				fileWriter.write(System.lineSeparator());
			}
			fileWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public static void main(String[] args) {
		new ExpectedCluster();
	}

}
