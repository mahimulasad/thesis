package thesis;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.BorderFactory;
import javax.swing.BoundedRangeModel;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.KeyStroke;
import javax.swing.ScrollPaneConstants;

public class VisualizeClusteringResult extends JFrame{
	
	private static final long serialVersionUID = 1L;
	
	private final String UP = "UP";
	private final String DOWN = "DOWN";
	private final String LEFT = "LEFT";
	private final String RIGHT = "RIGHT";


	public VisualizeClusteringResult() {
		super();
		this.setTitle("Clusterd Face");
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(700, 600);
		this.setLocation(205, 100);
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
		
		
		File clusterInfo = new File(Path.INFO+"clusterInfo.txt");
		try {
			Scanner cin = new Scanner(clusterInfo);
			while (cin.hasNext()) {//check for next cluster
				String clusterLabelString = cin.nextLine();
				JLabel clusterNumberLabel = new JLabel(clusterLabelString);
				clusterNumberLabel.setFont(new Font(clusterNumberLabel.getFont().getName(), Font.BOLD, 20));
				clusterNumberLabel.setForeground(Color.RED);
				//clusterNumberLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
				mainPanel.add(clusterNumberLabel);
				JPanel imagePanel = new JPanel();
				imagePanel.setLayout(new FlowLayout());
				while (cin.hasNextLine()) {//check number of image in cluster
					//each cluster is separated by a new line
					String info = cin.nextLine();
					if (info.equals("")) {
						break;
					}
					ImageIcon image = new ImageIcon(Path.DETECTED_FACE+info);
					JLabel imageLabel = new JLabel(image);
					imageLabel.setBorder(BorderFactory.createMatteBorder(3, 3, 3, 3, Color.BLACK));
					imagePanel.add(imageLabel);
				}
				mainPanel.add(imagePanel);
			}
			cin.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		JScrollPane mainScrollPane = new JScrollPane(mainPanel);
		mainScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		mainScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		
		InputMap inputMap = mainScrollPane.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
        ActionMap actionMap = mainScrollPane.getActionMap();
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_UP,0), UP);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_DOWN,0), DOWN);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, 0), LEFT);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, 0), RIGHT);
        
        int scrollableIncrement =50;
        actionMap.put(UP, new UpDownAction(UP, mainScrollPane.getVerticalScrollBar().getModel(), scrollableIncrement));
        actionMap.put(DOWN, new UpDownAction(DOWN, mainScrollPane.getVerticalScrollBar().getModel(), scrollableIncrement));
        actionMap.put(LEFT, new LeftRightAction(LEFT, mainScrollPane.getHorizontalScrollBar().getModel(), scrollableIncrement));
        actionMap.put(RIGHT, new LeftRightAction(RIGHT, mainScrollPane.getHorizontalScrollBar().getModel(), scrollableIncrement));
		
		add(mainScrollPane);
	}
	
    private class UpDownAction extends AbstractAction {
    	
		private static final long serialVersionUID = 1L;
		private BoundedRangeModel verticalScrollBarModel;
    	private int scrollableIncrement;
    	
    	public UpDownAction(String name, BoundedRangeModel verticalScrollBarModel, int scrollableIncrement) {
    		super(name);
    		this.verticalScrollBarModel = verticalScrollBarModel;
    		this.scrollableIncrement = scrollableIncrement;
    	}

		@Override
		public void actionPerformed(ActionEvent e) {
			String name = getValue(AbstractAction.NAME).toString();
			int value = verticalScrollBarModel.getValue();
			if (name.equals(UP)) {
				value -= scrollableIncrement;
				verticalScrollBarModel.setValue(value);
			}
			else if (name.equals(DOWN)) {
				value += scrollableIncrement;
				verticalScrollBarModel.setValue(value);
			}
		}
    	
    }
    
    private class LeftRightAction extends AbstractAction {
    	
		private static final long serialVersionUID = 1L;
		private BoundedRangeModel horizontalScrollBarModel;
    	private int scrollableIncrement;
    	
    	public LeftRightAction(String name, BoundedRangeModel horizontalScrollBarModel, int scrollableIncrement) {
    		super(name);
    		this.horizontalScrollBarModel = horizontalScrollBarModel;
    		this.scrollableIncrement = scrollableIncrement;
    	}

		@Override
		public void actionPerformed(ActionEvent e) {
			String name = getValue(AbstractAction.NAME).toString();
			int value = horizontalScrollBarModel.getValue();
			if (name.equals(LEFT)) {
				value -= scrollableIncrement;
				horizontalScrollBarModel.setValue(value);
			}
			else if (name.equals(RIGHT)) {
				value += scrollableIncrement;
				horizontalScrollBarModel.setValue(value);
			}
		}
    	
    }


	public static void main(String[] args) {
		new VisualizeClusteringResult();
	}

}
