/*
 * This program Run the face detection program (mainly written in C++ and opencv)
 * If the .exe file doesn't already exists, it should be created first
 * It stores the cropped image of face and the gray scale value of the cropped image
 */
package thesis;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;

public class FaceDetection {
	
	public FaceDetection() {
	}
	
	public void run() {
		
		String command = "cpp-source/face_detect_and_store.exe";
		String path = "cpp-source/";
		Process p;
		try {
			p = Runtime.getRuntime().exec(command,null, new File(path));
			p.waitFor();
			BufferedReader reader = 
                           new BufferedReader(new InputStreamReader(p.getInputStream()));

			String line = "";			
			while ((line = reader.readLine())!= null) {
				System.out.println(line);
			}

		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("\n.exe file not found. Create it manually before running the program.\n");
		}

		
	}

}
