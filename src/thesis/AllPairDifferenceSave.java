package thesis;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class AllPairDifferenceSave {
	
	private File folder;
	private File[] files;
	
	private int[][] lbpDistance;
	private int[][] lbpEucledianDistance;

	public AllPairDifferenceSave() {
		
		folder = new File(Path.DETECTED_FACE);
		files = folder.listFiles();
		
		System.out.println("Writing names in file");
		File nameFile = new File(Path.DIFFERENCE+"name.txt");
		try {
			FileWriter nameFileWriter = new FileWriter(nameFile);
			for (int i = 0; i < files.length; i++) {
				nameFileWriter.write(files[i].getName()+System.lineSeparator());
			}
			nameFileWriter.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		lbpDistance = new int[files.length][files.length];
		lbpEucledianDistance = new int[files.length][files.length];
		
		for (int i = 0; i < files.length; i++) {
			for (int j = i+1; j < files.length; j++) {
				Difference difference = new Difference(files[i], files[j], 
						FileType.DETECTED_FACE_IMAGE, FileType.SAVED_DIFFERENCE);
				System.out.println(i+" "+j+" "+difference.getTotalDistance()+" "+
							difference.getTotalEucledianDistance()+" "+
							files[i].getName()+" "+files[j].getName());
				
				lbpDistance[i][j] = difference.getTotalDistance();
				lbpEucledianDistance[i][j] = difference.getTotalEucledianDistance();
			}
		}
		
		System.out.println("Writing lbp distance into file...");
		File lbpDistanceFile = new File(Path.DIFFERENCE+"lbp_distance.txt");
		try {
			FileWriter lbpDistanceWriter = new FileWriter(lbpDistanceFile);
			for (int i = 0; i < files.length; i++) {
				for (int j = i+1; j < files.length; j++) {
					lbpDistanceWriter.write(lbpDistance[i][j]+" ");
				}
				lbpDistanceWriter.write(System.lineSeparator());
			}
			lbpDistanceWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("Done");
		
		System.out.println("Writing lbp eucledian distance into file...");
		File lbpEucledianDistanceFile = new File(Path.DIFFERENCE+"lbp_eucledian_distance.txt");
		try {
			FileWriter lbpEucledianDistanceWriter = new FileWriter(lbpEucledianDistanceFile);
			for (int i = 0; i < files.length; i++) {
				for (int j = i+1; j < files.length; j++) {
					lbpEucledianDistanceWriter.write(lbpEucledianDistance[i][j]+" ");
				}
				lbpEucledianDistanceWriter.write(System.lineSeparator());
			}
			lbpEucledianDistanceWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("Done");

	}
	
	public static void main(String[] args) {
		new AllPairDifferenceSave();
	}

}
