package thesis;

import java.io.File;
import java.util.ArrayList;

public class DeleteOldFiles {

	public DeleteOldFiles() {
	}
	
	public void run() {
		ArrayList<String> deleteFolderList = new ArrayList<>();
		deleteFolderList.add(Path.INFO);
		deleteFolderList.add(Path.DETECTED_FACE);
		deleteFolderList.add(Path.DETECTED_FACE_VALUE);
		deleteFolderList.add(Path.DETECTED_FACE_LBP);
		deleteFolderList.add(Path.LBP_FEATURE_VECTOR);
		
		for (String string : deleteFolderList) {
			File folder = new File(string);
			File files[] = folder.listFiles();
			for (File file : files) {
				file.delete();
			}
		}
	}
	
	public static void main(String[] args) {
		new DeleteOldFiles().run();
	}
}
