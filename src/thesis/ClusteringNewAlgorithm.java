package thesis;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class ClusteringNewAlgorithm {
	
	private int numberOfFace;
	private File[] faces; 
	private int representative[] ;
	
	private Map<String, String> personMap;
	
	private Map<Integer, ArrayList<String>> clusterMap;
	
	private Map<Integer, Set<Integer>> enemyMap;
	
	public ClusteringNewAlgorithm() {
		File folder = new File(Path.DETECTED_FACE);
		faces = folder.listFiles();
		numberOfFace = faces.length;
		
		initRepresentative();
		initPersonName();
		
		run();
		calculateFinalCluster();
		saveCluster();
		
		
	}
	
	private void initRepresentative() {
		representative = new int[numberOfFace];
		for (int i=0; i<numberOfFace ;i++)
			representative[i] = i;
	}
	
	private void initPersonName() {
		//map name of person for each file
		personMap = new HashMap<String, String>();
		for (int i = 0; i < faces.length; i++) {
			personMap.put(faces[i].getName(), faces[i].getName().substring(0, faces[i].getName().indexOf(".")));
		}
	}
	
	private boolean isSamePerson(String person1, String person2) {
		return personMap.get(person1).equals(personMap.get(person2));
	}
	
	private int findRepresentative(int n) {
		if (representative[n]==n) {
			return n;
		}
		//n er representative ki n?
		//jehetu ager na
		//n er new representative hobe, (n er representative) er representative
		representative[n]= findRepresentative(representative[n]);
		return representative[n];
	}
	
	private void union(int a, int b) {
		int u = findRepresentative(a);
		int v = findRepresentative(b);
		//System.out.println("Union: "+a+"("+findRepresentative(a)+") "+b+"("+findRepresentative(b)+")");
		if(u!=v){
			representative[v] =u ;
		}
		//System.out.println("after: "+a+"("+findRepresentative(a)+") "+b+"("+findRepresentative(b)+")");
	}
	
	public void run() {
		enemyMap = new HashMap<Integer, Set<Integer>>();
		ArrayList<ClusterInfo> allCluster = new ArrayList<ClusterInfo>();
		ArrayList<ClusterInfo> mixedCluster = new ArrayList<ClusterInfo>();
		
		//save every possible pair of image
		
		new LoadDifference();
		
		Scanner cin = new Scanner(System.in);
		for (int i = 0; i < faces.length; i++) {
			for (int j = i+1; j < faces.length; j++) {
				Difference difference = new Difference(faces[i], faces[j], 
						FileType.DETECTED_FACE_IMAGE, FileType.SAVED_DIFFERENCE);
				ClusterInfo clusterInfo = 
						new ClusterInfo(i, j, difference.getTotalDistance(),
								difference.getTotalEucledianDistance());
				allCluster.add(clusterInfo);				
			}
			System.out.println("Making every possible pair: iteration: "+i);
		}
		
		//sort all cluster pair, the rule of agglomerative clustering is to take 
		//the pair where the difference is minimum
		Collections.sort(allCluster);
				
		//for all pair
		int counter= 1;
		for (ClusterInfo clusterInfo : allCluster) {
			String name1 = faces[clusterInfo.getIndex1()].getName();
			String name2 = faces[clusterInfo.getIndex2()].getName();
			boolean equal = isSamePerson(name1, name2);
			System.out.println("["+counter++ +"] " +clusterInfo+"   "+(equal?"1":"0")+"   "+name1 +" "+name2);
			
			if (clusterInfo.getTotalDistance()<Threshold.LOW) {
				//System.out.println("same person");
				union(clusterInfo.getIndex1(), clusterInfo.getIndex2());
			}
			else if (clusterInfo.getTotalDistance()>Threshold.HIGH) {
				//add to enemy list
				//System.out.println("different person");
				addEnemy(clusterInfo.getIndex1(), clusterInfo.getIndex2());
			}
			else {
				//add to mixed cluster
				//System.out.println("skip now");
				mixedCluster.add(clusterInfo);
			}
		}
		
		//process mixed cluster
		System.out.println("\n\n\n\n\nmixed: "+mixedCluster.size());
		//Collections.sort(mixedCluster);//no need to sort, as allCluster is already sorted
		counter =1;
		for (ClusterInfo clusterInfo : mixedCluster) {
			String name1 = faces[clusterInfo.getIndex1()].getName();
			String name2 = faces[clusterInfo.getIndex2()].getName();
			boolean isSamePerson = isSamePerson(name1, name2);
			System.out.println("mixed["+counter++ +"]: "+clusterInfo+"   "+(isSamePerson?"1":"0")+"   "+name1 +" "+name2);
			//System.out.println(isEnemy(clusterInfo.getIndex1(), clusterInfo.getIndex2()));
			
			if (!isEnemy(clusterInfo.getIndex1(), clusterInfo.getIndex2())) {
				union(clusterInfo.getIndex1(), clusterInfo.getIndex2());
			}

//			else {
//				//don't need to do this, as they are already in enemy list
//				//addEnemy(clusterInfo.getIndex1(), clusterInfo.getIndex2());
//			}
			
			//System.out.println("\n\n");
		}
		
		cin.close();
	}
	
	private void addEnemy(int a, int b) {
		int u = findRepresentative(a);
		int v = findRepresentative(b);
		
		Set<Integer> uEnemy;
		if (enemyMap.containsKey(u)) {
			uEnemy = enemyMap.get(u);
		}
		else {
			uEnemy = new HashSet<Integer>();
		}
		
		Set<Integer> vEnemy;
		if (enemyMap.containsKey(v)) {
			vEnemy = enemyMap.get(v);
		}
		else {
			vEnemy = new HashSet<Integer>();
		}
		
		//System.out.println("Enemy: "+a+"("+findRepresentative(a)+") "+b+"("+findRepresentative(b)+")");
		//System.out.println("Enemy name: "+faces[a].getName()+"("+faces[findRepresentative(a)].getName()+") "+
		//			faces[b].getName()+"("+faces[findRepresentative(b)].getName()+")");
		//System.out.println(u+": "+uEnemy);
		//System.out.println(v+": "+vEnemy);
		uEnemy.add(v);
		vEnemy.add(u);
		
		uEnemy = uniqueEnemyList(uEnemy);
		vEnemy = uniqueEnemyList(vEnemy);
		
		enemyMap.put(u, uEnemy);
		enemyMap.put(v, vEnemy);
		
		//System.out.println("After:");
		//System.out.println(u+": "+enemyMap.get(findRepresentative(a)));
		//System.out.println(v+": "+enemyMap.get(findRepresentative(b)));
	}
	
	private Set<Integer> uniqueEnemyList(Set<Integer> enemy) {
		Set<Integer> ret = new HashSet<Integer>();
		for (Integer integer : enemy) {
			ret.add(findRepresentative(integer));
		}
		return ret;
	}

	private boolean isEnemy(int a, int b) {
		int u = findRepresentative(a);
		int v = findRepresentative(b);
		
		Set<Integer> uEnemy;
		if (enemyMap.containsKey(u)) {
			uEnemy = enemyMap.get(u);
		}
		else {
			uEnemy = new HashSet<Integer>();
		}
		
		Set<Integer> vEnemy;
		if (enemyMap.containsKey(v)) {
			vEnemy = enemyMap.get(v);
		}
		else {
			vEnemy = new HashSet<Integer>();
		}

		uEnemy = uniqueEnemyList(uEnemy);
		vEnemy = uniqueEnemyList(vEnemy);
		
		enemyMap.put(u, uEnemy);
		enemyMap.put(v, vEnemy);

		//System.out.println("Person: "+a+"("+findRepresentative(a)+") "+b+"("+findRepresentative(b)+")");
		//System.out.println("Person name: "+faces[a].getName()+"("+faces[findRepresentative(a)].getName()+") "+
		//			faces[b].getName()+"("+faces[findRepresentative(b)].getName()+")");
		//System.out.println(u+": "+uEnemy);
		//System.out.println(v+": "+vEnemy);
		
		if (uEnemy.contains(v)) {
			return true;
		}
		if (vEnemy.contains(u)) {//for security, so that don't miss by chance
			return true;
		}
		return false;
	}
	
	public void calculateFinalCluster() {
		
		//path compression
		//changing the representative
		//if not already minimized
		for (int i = 0; i < representative.length; i++) {
			if (representative[i]!=i) {
				representative[i] = findRepresentative(i);
			}
		}
		
		//make final cluster
		clusterMap = new HashMap<Integer, ArrayList<String>>();
		for (int i = 0; i < representative.length; i++) {
			int representativeValue = representative[i];
			
			if (clusterMap.containsKey(representativeValue)) {
				ArrayList<String> arrayList = clusterMap.get(representativeValue);
				arrayList.add(faces[i].getName().toString());
				clusterMap.put(representativeValue, arrayList);
			}
			else {
				ArrayList<String> arrayList = new ArrayList<String>();
				arrayList.add(faces[i].getName().toString());
				clusterMap.put(representativeValue, arrayList);
			}
		}
	}
	
	public Map<Integer, ArrayList<String>> getClusterMap() {
		return clusterMap;
	}
	
	public ArrayList< ArrayList<String>> getArrayListOfCluster() {
		ArrayList<ArrayList<String>> ret = new ArrayList<ArrayList<String>>();
		for (Integer integer : clusterMap.keySet()) {
			ret.add(clusterMap.get(integer));
		}
		return ret;
	}
	
	public void saveCluster() {
		File clusterOutputFile = new File(Path.INFO+"clusterInfoNewAlgo.txt");
		try {
			FileWriter fileWriter = new FileWriter(clusterOutputFile);
			int clusterNo=1;
			for (Integer integer : clusterMap.keySet()) {
				fileWriter.write("Cluster "+clusterNo++ +": "+System.lineSeparator());
				ArrayList<String> arrayList = clusterMap.get(integer);
				for (String string : arrayList) {
					fileWriter.write(string+System.lineSeparator());
				}
				fileWriter.write(System.lineSeparator());
			}
			fileWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
		
	public static void main(String args[]) {
		ClusteringNewAlgorithm clustering = new ClusteringNewAlgorithm();
		System.out.println(clustering.getArrayListOfCluster());
	}
	
}
