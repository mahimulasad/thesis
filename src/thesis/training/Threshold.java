package thesis.training;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import thesis.ClusterInfo;
import thesis.Difference;
import thesis.FileType;
import thesis.LoadDifference;
import thesis.Path;

public class Threshold {
	
	private File folder;
	private File[] files;
	
	Map<String, String> personMap;
	
	public Threshold() {
		folder = new File(Path.DETECTED_FACE);
		files = folder.listFiles();
		
		//map name of person for each file
		personMap = new HashMap<String, String>();
		for (int i = 0; i < files.length; i++) {
			personMap.put(files[i].getName(), 
					files[i].getName().substring(0,files[i].getName().indexOf(".")));
		}
		
		new LoadDifference();
		ArrayList<ClusterInfo> clusterArrayList = new ArrayList<ClusterInfo>();
		for (int i = 0; i < files.length; i++) {
			for (int j = i+1; j < files.length; j++) {
				Difference difference = new Difference(files[i], files[j], 
						FileType.DETECTED_FACE_IMAGE, FileType.SAVED_DIFFERENCE);
				boolean isSamePerson = samePerson(files[i].getName(), files[j].getName()); 
				ClusterInfo clusterInfo = new ClusterInfo(i, j, difference.getTotalDistance(),
						difference.getTotalEucledianDistance(), isSamePerson);
				clusterArrayList.add(clusterInfo);
			}
			System.out.println("Iteration: "+i);
		}
		
		Collections.sort(clusterArrayList);
		
		int index = -1;
		for (int i = 0; i < clusterArrayList.size(); i++) {
			//System.out.println(clusterArrayList.get(i)+ " "+clusterArrayList.get(i).isSamePerson());
			//get the index when first false occurs in mixed group
			if (clusterArrayList.get(i).isSamePerson()==false) {
				index = i;
				break;
			}
		}
		
		int minThreshold = (clusterArrayList.get(index).getTotalDistance() +
							clusterArrayList.get(index-1).getTotalDistance() +1)/2;
		System.out.println(minThreshold);
		
		index = -1;
		for (int i = clusterArrayList.size()-1; i >= 0; i--) {
			//System.out.println(clusterArrayList.get(i)+ " "+clusterArrayList.get(i).isSamePerson());
			//get the index when first true occurs in mixed group
			//iterating from last
			if (clusterArrayList.get(i).isSamePerson()==true) {
				index = i;
				break;
			}
		}
		int maxThreshold = (clusterArrayList.get(index).getTotalDistance() +
				clusterArrayList.get(index+1).getTotalDistance() +1)/2;
		System.out.println(maxThreshold);
		
		//write in file
		try {
			File file = new File(Path.TRAINING+"threshold.txt");
			FileWriter fileWriter = new FileWriter(file);
			fileWriter.write(minThreshold+System.lineSeparator());
			fileWriter.write(maxThreshold+System.lineSeparator());
			fileWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	private boolean samePerson(String person1, String person2) {
		return personMap.get(person1).equals(personMap.get(person2));
	}

	public static void main(String[] args) {
		new Threshold();
	}

}
