package thesis.training;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import thesis.Difference;
import thesis.FileType;
import thesis.LoadDifference;
import thesis.Path;

public class TrainingFeature {
	
	private File folder;
	private File[] files;
	
	Map<String, String> personMap;
	
	public TrainingFeature() throws IOException {
		folder = new File(Path.DETECTED_FACE);
		files = folder.listFiles();
		
		//map name of person for each file
		personMap = new HashMap<String, String>();
		for (int i = 0; i < files.length; i++) {
			personMap.put(files[i].getName(), 
					files[i].getName().substring(0,files[i].getName().indexOf(".")));
		}
		
		System.out.println(personMap);
		
		File samePersonDistanceFile = new File(Path.TRAINING+"same_person_dis.txt");
		File samePersonEucledianDistanceFile = new File(Path.TRAINING + "same_person_eu_dis.txt");
		File differentPersonDistanceFile = new File(Path.TRAINING + "different_person_dis.txt");
		File differentPersonEucledianDistanceFile = new File(Path.TRAINING + "different_person_eu_dis.txt");
				
		BufferedWriter samePersonDistanceWriter = new BufferedWriter(new FileWriter(samePersonDistanceFile));
		BufferedWriter samePersonEucledianDistanceWriter = new BufferedWriter(new FileWriter(samePersonEucledianDistanceFile));
		BufferedWriter differentPersonDistanceWriter = new BufferedWriter(new FileWriter(differentPersonDistanceFile));
		BufferedWriter differentPersonEucledianDistanceWriter = new BufferedWriter(new FileWriter(differentPersonEucledianDistanceFile));
		
		new LoadDifference();
		for (int i = 0; i < files.length; i++) {
			for (int j = i+1; j < files.length; j++) {
				Difference difference = new Difference(files[i], files[j], 
						FileType.DETECTED_FACE_IMAGE, FileType.SAVED_DIFFERENCE);
				boolean isSamePerson = samePerson(files[i].getName(), files[j].getName()); 
				System.out.println(i+" "+j+" "+
							difference.getTotalDistance()+" "+
							difference.getTotalEucledianDistance()+" " +
							(isSamePerson?"1":"0") + "   "+
							files[i].getName()+" "+files[j].getName());
				if (isSamePerson) {
					samePersonDistanceWriter.write(difference.getTotalDistance()+"\r\n");
					samePersonEucledianDistanceWriter.write(difference.getTotalEucledianDistance()+"\r\n");
				}
				else {
					differentPersonDistanceWriter.write(difference.getTotalDistance()+"\r\n");
					differentPersonEucledianDistanceWriter.write(difference.getTotalEucledianDistance()+"\r\n");
				}
			}
		}
		
		samePersonDistanceWriter.close();
		samePersonEucledianDistanceWriter.close();
		differentPersonDistanceWriter.close();
		differentPersonEucledianDistanceWriter.close();
	}
	
	private boolean samePerson(String person1, String person2) {
		return personMap.get(person1).equals(personMap.get(person2));
	}

	public static void main(String[] args) {
		try {
			new TrainingFeature();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
