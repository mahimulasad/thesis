package thesis.training;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import thesis.ClusterInfo;
import thesis.Difference;
import thesis.FileType;
import thesis.LoadDifference;
import thesis.Path;

public class AllTrainingInfoSave {
	private File folder;
	private File[] files;
	
	Map<String, String> personMap;
	
	public AllTrainingInfoSave() {
		folder = new File(Path.DETECTED_FACE);
		files = folder.listFiles();
		
		//map name of person for each file
		personMap = new HashMap<String, String>();
		for (int i = 0; i < files.length; i++) {
			personMap.put(files[i].getName(), 
					files[i].getName().substring(0,files[i].getName().indexOf(".")));
		}
		
		ArrayList<ClusterInfo> clusterArrayList = new ArrayList<ClusterInfo>();
		ArrayList<ClusterInfo> positiveCluster = new ArrayList<ClusterInfo>();
		ArrayList<ClusterInfo> negativeCluster = new ArrayList<ClusterInfo>();
		
		File samePersonDistanceFile = new File(Path.TRAINING+"same_person_dis.txt");
		File samePersonEucledianDistanceFile = new File(Path.TRAINING + "same_person_eu_dis.txt");
		File differentPersonDistanceFile = new File(Path.TRAINING + "different_person_dis.txt");
		File differentPersonEucledianDistanceFile = new File(Path.TRAINING + "different_person_eu_dis.txt");
				
		try {
			FileWriter samePersonDistanceWriter = new FileWriter(samePersonDistanceFile);
			FileWriter samePersonEucledianDistanceWriter = new FileWriter(samePersonEucledianDistanceFile);
			FileWriter differentPersonDistanceWriter = new FileWriter(differentPersonDistanceFile);
			FileWriter differentPersonEucledianDistanceWriter = new FileWriter(differentPersonEucledianDistanceFile);
			
			new LoadDifference();
			
			for (int i = 0; i < files.length; i++) {
				for (int j = i+1; j < files.length; j++) {
					Difference difference = new Difference(files[i], files[j], 
							FileType.DETECTED_FACE_IMAGE, FileType.SAVED_DIFFERENCE);
					boolean isSamePerson = samePerson(files[i].getName(), files[j].getName()); 
					ClusterInfo clusterInfo = new ClusterInfo(i, j, difference.getTotalDistance(),
							difference.getTotalEucledianDistance(), isSamePerson);
					clusterArrayList.add(clusterInfo);
					if (isSamePerson) {
						positiveCluster.add(clusterInfo);
						samePersonDistanceWriter.write(difference.getTotalDistance()+"\r\n");
						samePersonEucledianDistanceWriter.write(difference.getTotalEucledianDistance()+"\r\n");
					}
					else {
						negativeCluster.add(clusterInfo);
						differentPersonDistanceWriter.write(difference.getTotalDistance()+"\r\n");
						differentPersonEucledianDistanceWriter.write(difference.getTotalEucledianDistance()+"\r\n");
					}
				}
				System.out.println("Iteration: "+i);
			}
			
			samePersonDistanceWriter.close();
			samePersonEucledianDistanceWriter.close();
			differentPersonDistanceWriter.close();
			differentPersonEucledianDistanceWriter.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		
		System.out.println("Sorting all cluster: "+clusterArrayList.size());
		Collections.sort(clusterArrayList);
		System.out.println("Sorting positive cluster: "+positiveCluster.size());
		Collections.sort(positiveCluster);
		System.out.println("Sorting negative cluster: "+negativeCluster.size());
		Collections.sort(negativeCluster);
		
		
		//write in positive file
		System.out.println("Writing into cluster_positive.txt file...");
		try {
			File posFile = new File(Path.TRAINING+"cluster_positive.txt");
			FileWriter fileWriter = new FileWriter(posFile);
			for (ClusterInfo clusterInfo : positiveCluster) {
//				System.out.println(clusterInfo+" "+clusterInfo.isSamePerson()+" "
//							+files[clusterInfo.getIndex1()].getName()+" "+files[clusterInfo.getIndex2()].getName());
			fileWriter.write(clusterInfo+" "+clusterInfo.isSamePerson()+" "
							+files[clusterInfo.getIndex1()].getName()+" "+files[clusterInfo.getIndex2()].getName()
							+System.lineSeparator());
			}
			fileWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("Done");
		
		//write in negative file
		System.out.println("Writing into cluster_negative.txt file...");
		try {
			File negFile = new File(Path.TRAINING+"cluster_negative.txt");
			FileWriter fileWriter = new FileWriter(negFile);
			for (ClusterInfo clusterInfo : negativeCluster) {
			fileWriter.write(clusterInfo+" "+clusterInfo.isSamePerson()+" "
							+files[clusterInfo.getIndex1()].getName()+" "+files[clusterInfo.getIndex2()].getName()
							+System.lineSeparator());
			}
			fileWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("Done");
		
		System.out.println("Calculating threshold value...");
		int index = -1;
		for (int i = 0; i < clusterArrayList.size(); i++) {
			//System.out.println(clusterArrayList.get(i)+ " "+clusterArrayList.get(i).isSamePerson());
			//get the index when first false occurs in mixed group
			if (clusterArrayList.get(i).isSamePerson()==false) {
				index = i;
				break;
			}
		}
		
		int minThreshold = (clusterArrayList.get(index).getTotalDistance() +
							clusterArrayList.get(index-1).getTotalDistance() +1)/2;
		System.out.println(minThreshold);
		
		index = -1;
		for (int i = clusterArrayList.size()-1; i >= 0; i--) {
			//System.out.println(clusterArrayList.get(i)+ " "+clusterArrayList.get(i).isSamePerson());
			//get the index when first true occurs in mixed group
			//iterating from last
			if (clusterArrayList.get(i).isSamePerson()==true) {
				index = i;
				break;
			}
		}
		int maxThreshold = (clusterArrayList.get(index).getTotalDistance() +
				clusterArrayList.get(index+1).getTotalDistance() +1)/2;
		System.out.println(maxThreshold);
		
		//write in file
		try {
			File file = new File(Path.TRAINING+"threshold.txt");
			FileWriter fileWriter = new FileWriter(file);
			fileWriter.write(minThreshold+System.lineSeparator());
			fileWriter.write(maxThreshold+System.lineSeparator());
			fileWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("Done");

	}
	
	private boolean samePerson(String person1, String person2) {
		return personMap.get(person1).equals(personMap.get(person2));
	}

	public static void main(String[] args) {
		new AllTrainingInfoSave();
	}

}
