package thesis.training;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import thesis.ClusterInfo;
import thesis.Difference;
import thesis.FileType;
import thesis.LoadDifference;
import thesis.Path;

public class SelectedCluster {
	
	private File folder;
	private File[] files;
	
	Map<String, String> personMap;
	
	public SelectedCluster() {
		folder = new File(Path.DETECTED_FACE);
		files = folder.listFiles();
		
		//map name of person for each file
		personMap = new HashMap<String, String>();
		for (int i = 0; i < files.length; i++) {
			personMap.put(files[i].getName(), 
					files[i].getName().substring(0,files[i].getName().indexOf(".")));
		}
		
		ArrayList<String> inputName = new ArrayList<String>();
		System.out.println("Enter name of person in each line [input terminates with blank line]: ");
		Scanner cin = new Scanner(System.in);
		while (cin.hasNextLine()) {
			String input = cin.nextLine();
			if (input.equals("")) {
				break;
			}
			inputName.add(input);
		}
		cin.close();
				
		ArrayList<File> selectedFile = new ArrayList<File>();
		for (int i = 0; i < files.length; i++) {
			if (inputName.contains(personMap.get(files[i].getName()))) {
				selectedFile.add(files[i]);
			}
		}
		
		System.out.println("selected file size:"+selectedFile.size());
		
		new LoadDifference();
		
		ArrayList<ClusterInfo> allClusterInfo = new ArrayList<ClusterInfo>();
	
		for (int i = 0; i < selectedFile.size(); i++) {
			for (int j = i+1; j < selectedFile.size(); j++) {
				Difference difference = new Difference(selectedFile.get(i), selectedFile.get(j), 
						FileType.DETECTED_FACE_IMAGE, FileType.SAVED_DIFFERENCE);
				boolean isSamePerson = samePerson(selectedFile.get(i).getName(), selectedFile.get(j).getName()); 				
				ClusterInfo clusterInfo = new ClusterInfo(i, j, difference.getTotalDistance(),
						difference.getTotalEucledianDistance(), isSamePerson);
				
				allClusterInfo.add(clusterInfo);
			}
		}
		
		Collections.sort(allClusterInfo);
		for (ClusterInfo clusterInfo : allClusterInfo) {
			System.out.println(clusterInfo+"   "+clusterInfo.isSamePerson()+"   "+
						selectedFile.get(clusterInfo.getIndex1()).getName()+" "+
						selectedFile.get(clusterInfo.getIndex2()).getName());
		}
	}
	
	private boolean samePerson(String person1, String person2) {
		return personMap.get(person1).equals(personMap.get(person2));
	}

	
	public static void main(String[] args) {
		new SelectedCluster();
	}

}
