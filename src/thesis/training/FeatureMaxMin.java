package thesis.training;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import thesis.Difference;
import thesis.FileType;
import thesis.LoadDifference;
import thesis.Path;

public class FeatureMaxMin {
	private File folder;
	private File[] files;

	Map<String, String> personMap;
	Map<String, Integer> nameMap;
	Map<Integer, String> indexMap;

	public FeatureMaxMin() {
		folder = new File(Path.DETECTED_FACE);
		files = folder.listFiles();

		// map name of person for each file
		personMap = new HashMap<String, String>();
		nameMap = new HashMap<String, Integer>();
		indexMap = new HashMap<Integer, String>();
		int personNo = 0;

		for (int i = 0; i < files.length; i++) {
			String name = files[i].getName().substring(0, files[i].getName().indexOf("."));
			personMap.put(files[i].getName(), name);
			if (!nameMap.containsKey(name)) {
				nameMap.put(name, personNo);
				indexMap.put(personNo, name);
				personNo++;
			}
		}

		int max[][] = new int[personNo][personNo];// init to 0
		int min[][] = new int[personNo][personNo];
		for (int i = 0; i < min.length; i++) {
			Arrays.fill(min[i], 1000000);
		}
		new LoadDifference();

		for (int i = 0; i < files.length; i++) {
			for (int j = i + 1; j < files.length; j++) {
				Difference difference = new Difference(files[i], files[j], FileType.DETECTED_FACE_IMAGE,
						FileType.SAVED_DIFFERENCE);
				int a = nameMap.get(personMap.get(files[i].getName()));
				int b = nameMap.get(personMap.get(files[j].getName()));
				max[a][b] = Math.max(max[a][b], difference.getTotalDistance());
				max[b][a] = Math.max(max[b][a], difference.getTotalDistance());
				min[a][b] = Math.min(min[a][b], difference.getTotalDistance());
				min[b][a] = Math.min(min[b][a], difference.getTotalDistance());
			}
			System.out.println("Iteration: " + i);
		}

		// write in feature max file
		System.out.println("Writing into feature_max.csv file...");
		try {
			File maxFile = new File(Path.TRAINING + "feature_max.csv");
			FileWriter fileWriter = new FileWriter(maxFile);

			for (int i = 0; i < max.length; i++) {
				for (int j = 0; j < max[i].length; j++) {
					fileWriter.write(max[i][j] + ",");
				}
				fileWriter.write(System.lineSeparator());
			}

			//summary
			fileWriter.write("Summary: Maximum in same pair of image," + System.lineSeparator());
			for (int i = 0; i < max.length; i++) {
				fileWriter.write(max[i][i] + ",");
			}
			fileWriter.write(System.lineSeparator());
			
			for (int i = 0; i < max.length; i++) {
				fileWriter.write(indexMap.get(i) + ",");
			}
			

			fileWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("Done");

		// write in feature max file
		System.out.println("Writing into feature_min.csv file...");
		try {
			File maxFile = new File(Path.TRAINING + "feature_min.csv");
			FileWriter fileWriter = new FileWriter(maxFile);

			for (int i = 0; i < min.length; i++) {
				for (int j = 0; j < min[i].length; j++) {
					fileWriter.write(min[i][j] + ",");
				}
				fileWriter.write(System.lineSeparator());
			}
			//summary
			fileWriter.write("Summary: Minimum in different pair of image," + System.lineSeparator());
			int[] minIndex = new int[min.length];
			Arrays.fill(minIndex, -1);
			for (int i = 0; i < min.length; i++) {
				int minimumOfThisPerson = 9999999;
				for (int j = 0; j < min[i].length; j++) {
					if (i!=j) {
						if (min[i][j]<minimumOfThisPerson) {
							minimumOfThisPerson = min[i][j];
							minIndex[i] = j;
						}
					}
				}
				fileWriter.write(minimumOfThisPerson+",");
			}
			fileWriter.write(System.lineSeparator());
			for (int i = 0; i < max.length; i++) {
				fileWriter.write(indexMap.get(i) + ",");
			}
			fileWriter.write(System.lineSeparator());
			for (int i = 0; i < max.length; i++) {
				fileWriter.write(indexMap.get(minIndex[i]) + ",");
			}

			fileWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("Done");

	}

	public static void main(String[] args) {
		new FeatureMaxMin();
	}

}
