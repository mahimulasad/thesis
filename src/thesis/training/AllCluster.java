package thesis.training;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import thesis.ClusterInfo;
import thesis.Difference;
import thesis.FileType;
import thesis.LoadDifference;
import thesis.Path;

public class AllCluster {
	private File folder;
	private File[] files;
	
	Map<String, String> personMap;
	
	public AllCluster() {
		folder = new File(Path.DETECTED_FACE);
		files = folder.listFiles();
		
		//map name of person for each file
		personMap = new HashMap<String, String>();
		for (int i = 0; i < files.length; i++) {
			personMap.put(files[i].getName(), 
					files[i].getName().substring(0,files[i].getName().indexOf(".")));
		}
		
		ArrayList<ClusterInfo> positiveCluster = new ArrayList<ClusterInfo>();
		ArrayList<ClusterInfo> negativeCluster = new ArrayList<ClusterInfo>();
		
		new LoadDifference();
		
		for (int i = 0; i < files.length; i++) {
			for (int j = i+1; j < files.length; j++) {
				Difference difference = new Difference(files[i], files[j], 
						FileType.DETECTED_FACE_IMAGE, FileType.SAVED_DIFFERENCE);
				boolean isSamePerson = samePerson(files[i].getName(), files[j].getName()); 
				ClusterInfo clusterInfo = new ClusterInfo(i, j, difference.getTotalDistance(),
						difference.getTotalEucledianDistance(), isSamePerson);
				if (isSamePerson) {
					positiveCluster.add(clusterInfo);
				}
				else {
					negativeCluster.add(clusterInfo);
				}
			}
			System.out.println("Iteration: "+i);
		}
		
		System.out.println("Sorting positive cluster: "+positiveCluster.size());
		Collections.sort(positiveCluster);
		System.out.println("Sorting negative cluster: "+negativeCluster.size());
		Collections.sort(negativeCluster);
		
		
		//write in positive file
		System.out.println("Writing into cluster_positive.txt file...");
		try {
			File posFile = new File(Path.TRAINING+"cluster_positive.txt");
			FileWriter fileWriter = new FileWriter(posFile);
			for (ClusterInfo clusterInfo : positiveCluster) {
//				System.out.println(clusterInfo+" "+clusterInfo.isSamePerson()+" "
//							+files[clusterInfo.getIndex1()].getName()+" "+files[clusterInfo.getIndex2()].getName());
			fileWriter.write(clusterInfo+" "+clusterInfo.isSamePerson()+" "
							+files[clusterInfo.getIndex1()].getName()+" "+files[clusterInfo.getIndex2()].getName()
							+System.lineSeparator());
			}
			fileWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("Done");
		
		//write in negative file
		System.out.println("Writing into cluster_negative.txt file...");
		try {
			File negFile = new File(Path.TRAINING+"cluster_negative.txt");
			FileWriter fileWriter = new FileWriter(negFile);
			for (ClusterInfo clusterInfo : negativeCluster) {
			fileWriter.write(clusterInfo+" "+clusterInfo.isSamePerson()+" "
							+files[clusterInfo.getIndex1()].getName()+" "+files[clusterInfo.getIndex2()].getName()
							+System.lineSeparator());
			}
			fileWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("Done");

	}
	
	private boolean samePerson(String person1, String person2) {
		return personMap.get(person1).equals(personMap.get(person2));
	}

	public static void main(String[] args) {
		new AllCluster();
	}

}
