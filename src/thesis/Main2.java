package thesis;

public class Main2 {

	public static void main(String[] args) {
		
		System.out.println("Executing 2nd part...");
		System.out.println();
		
		//generate lbp value and lbp feature vector/template for each image
		System.out.println("Generating LBP value and LBP feature vector...");
		ImageToLBP imageToLBP = new ImageToLBP();
		imageToLBP.run();
		System.out.println();
		
		//Clustering process
		System.out.println("Clustering Process running...");
		new ClusteringAgglomerative();
		System.out.println();
		
		//visualize Clustering result
		System.out.println("Visualizing Clustering result...");
		new VisualizeClusteringResult();
		System.out.println();
		
		System.out.println("Program finished");
		
	}

}
