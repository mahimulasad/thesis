package thesis;

public class FileType {
	
	public static final int INPUT_IMAGE = 1;
	public static final int INFO_FILE = 2;
	public static final int DETECTED_FACE_IMAGE = 3;
	public static final int DETECTED_FACE_VALUE = 4;
	public static final int DETECTED_FACE_LBP = 5;
	public static final int LBP_FEATURE_VECTOR = 6;
	public static final int SAVED_DIFFERENCE = 7;

}
