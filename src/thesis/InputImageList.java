/*
 * This class create a list of input image (data/current/inputImage/)
 * save the list (InputImageList.txt) at data/current/info/
 */
package thesis;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class InputImageList {
	
	private FileWriter fw;
	
	public InputImageList() {
		
	}
	
	public void create() {
		try {
			File file = new File(Path.INFO+"InputImageList.txt");
			if (!file.exists()) {//if the file doesn't exist, create new file
				file.getParentFile().mkdirs();
				file.createNewFile();
			}
			fw = new FileWriter(file);
			traverse(Path.INPUT_IMAGE);
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void traverse(String folderName) {
		File folder = new File(folderName);
		for (File files : folder.listFiles()) {
			if (!files.isDirectory()) {
//					System.out.println(files.getName());
				try {
					fw.write(files.getPath().replaceAll("\\\\", "/")+"\r\n");
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			else {
				traverse(files.getPath());
			}
		}
		
	}
	
	
	public static void main(String args[]) {
		InputImageList inputImageList = new InputImageList();
		inputImageList.create();
	}

}
