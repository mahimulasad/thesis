package thesis;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class ResultCompare {

	File folder;
	File[] faces;
	Map<String, ArrayList<String>> map;
	Map<String, ArrayList<String>> clusteringNewAlgoResult;
	
	public ResultCompare() {
		folder = new File(Path.DETECTED_FACE);
		faces = folder.listFiles();
		map = new HashMap<String, ArrayList<String>>();
		clusteringNewAlgoResult = new HashMap<String, ArrayList<String>>();
		
		//load expected cluster
		for (File file : faces) {
			String name = getName(file.getName());
			ArrayList<String> temp;
			if (map.containsKey(name)) {
				temp = map.get(name);
			}
			else {
				temp = new ArrayList<String>();
			}
			temp.add(file.getName());
			map.put(name, temp);
		}
		
		//load cluster by clustering algorithm (new)
		File clusterInfoNewAlgo = new File(Path.INFO+"clusterInfoNewAlgo.txt");
		try {
			Scanner cin = new Scanner(clusterInfoNewAlgo);
			while (cin.hasNext()) {//check for next cluster
				cin.nextLine();//cluster label input
				ArrayList<String> temp = new ArrayList<>();
				while (cin.hasNextLine()) {//check number of image in cluster
					//each cluster is separated by a new line
					String info = cin.nextLine();
					if (info.equals("")) {
						break;
					}
					temp.add(info);
				}
				if (isCorrectCluster(temp)) {
					clusteringNewAlgoResult.put(getName(temp.get(0)), temp);
				}
				else {
					System.err.println("Problem in cluster: "+temp);
				}
			}
			cin.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		//sort
		Set<String> allName = map.keySet();
		ArrayList<String> allNameSorted = new ArrayList<>(allName);
		Collections.sort(allNameSorted);
		System.out.println(allNameSorted);

		//compare
		int counter = 1;
		for (String string : allNameSorted) {
			System.out.println("Cluster "+ counter++ +": "+string);
			System.out.println("Expected: "+map.get(string).size());
			System.out.println("Result  : "+clusteringNewAlgoResult.get(string).size());
			System.out.println();
		}
	}
	
	private String getName(String filename) {
		return filename.substring(0,filename.indexOf("."));
	}
	
	private boolean isCorrectCluster(ArrayList<String> files) {
		String clusterName = getName(files.get(0));
		for (int i = 1; i < files.size(); i++) {
			if (!clusterName.equals(getName(files.get(i)))) {
				return false;
			}
		}
		return true;
	}
	
	public static void main(String[] args) {
		new ResultCompare();
	}

}
