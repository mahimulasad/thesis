package thesis;

public class Path {
	
	public static String INFO = "data/current/info/";
	public static String INPUT_IMAGE = "data/current/input_image/";
	public static String DETECTED_FACE = "data/current/detected_face/";
	public static String DETECTED_FACE_VALUE = "data/current/detected_face_value/";
	public static String DETECTED_FACE_LBP = "data/current/detected_face_lbp/";
	public static String LBP_FEATURE_VECTOR = "data/current/lbp_feature_vector/";
	public static String TRAINING = "data/current/training/";
	public static String DIFFERENCE = "data/current/difference/";
	
}
