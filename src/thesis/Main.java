package thesis;

@Deprecated
public class Main {

	public static void main(String[] args) {
		
		//Delete old files
		System.out.println("Deleting old files");
		DeleteOldFiles deleteOldFiles = new DeleteOldFiles();
		deleteOldFiles.run();
		System.out.println();
		
		//create list of input image and save it to data/current/info/InputImageList.txt 
		System.out.println("Creating file list of input image...");
		InputImageList inputImageList = new InputImageList();
		inputImageList.create();
		System.out.println();

//		//This part is hidden so that the .exe file doesn't corrupt
//		//detect face using opencv
//		//save the image of cropped face
//		//save the value of cropped face image
//		System.out.println("Face detection process running...");
//		FaceDetection faceDetection = new FaceDetection();
//		faceDetection.run();
//		System.out.println();
		
		//generate lbp value and lbp feature vector/template for each image
		System.out.println("Generating LBP value and LBP feature vector...");
		ImageToLBP imageToLBP = new ImageToLBP();
		imageToLBP.run();
		System.out.println();
		
		//Clustering process
		System.out.println("Clustering Process running...");
		new ClusteringAgglomerative();
		System.out.println();
		
		//visualize Clustering result
		System.out.println("Visualizing Clustering result...");
		new VisualizeClusteringResult();
		System.out.println();
		
		System.out.println("Program finished");
		
	}

}
