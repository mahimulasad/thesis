/**
*** This program detect all the faces from input image folder
*** and visualize them
***/
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include <iostream>
#include <stdio.h>

using namespace std;
using namespace cv;

// Function Headers
void detectAndDisplay(Mat frame, string originalImageFileName);

// Global variables
// Copy this file from opencv/data/haarscascades to target folder
string face_cascade_name = "../trained_classifier/haarcascade_frontalface_alt.xml";
//string face_cascade_name = "D:/opencv/sources/data/lbpcascades/lbpcascade_frontalface.xml";
//string face_cascade_name = "D:/opencv/sources/data/lbpcascades/lbpcascade_profileface.xml";

CascadeClassifier face_cascade;
string window_name = "Capture - Face detection";
int filenumber; // Number of file to be saved
string filename;

// Function main
int main(void)
{
    freopen("../data/current/info/InputImageList.txt","r",stdin);
    // Load the cascade
    if (!face_cascade.load(face_cascade_name)){
        printf("--(!)Error loading classifier\n");
        return (-1);
    }

    string imageFileName;
    while(getline(cin, imageFileName))
    {
        if(!imageFileName.compare(""))
            break;

        // Read the image file
        string fullpath = "../" + imageFileName;
        Mat frame = imread(fullpath);
        // Apply the classifier to the frame
        if (!frame.empty()){
            detectAndDisplay(frame,imageFileName);
        }
        else{
            printf(" --(!) Invalid image %s\n",imageFileName.c_str());
        }

        int c = waitKey(0);//wait until any key is pressed
    }

    return 0;
}

// Function detectAndDisplay
void detectAndDisplay(Mat frame, string originalImageFileName)
{
    cout<<"processing image: "<<originalImageFileName<<endl;
    std::vector<Rect> faces;
    Mat frame_gray;
    Mat crop;
    Mat res;
    Mat gray;
    string text;
    stringstream sstm;

    cvtColor(frame, frame_gray, COLOR_BGR2GRAY);//convert RGB image to gray image
    equalizeHist(frame_gray, frame_gray);

    //output image size 64*64
    /** 64*64 is nearly perfect. compared with other resulation **/
    Size outputImageSize = Size(64,64);

    // Detect faces
    face_cascade.detectMultiScale(frame_gray, faces, 1.1, 2, 0 | CASCADE_SCALE_IMAGE, outputImageSize);
    //frame_gray ->input images
    //faces -> Vector of rectangles where each rectangle contains the detected object
    //1.1->scale factor
    //flag
    //minSize ->Minimum possible object size. Objects smaller than that are ignored.
    //maxSize � Maximum possible object size. Objects larger than that are ignored.

    // Set Region of Interest
    cv::Rect roi_b;
    cv::Rect roi_c;

    size_t ic = 0; // ic is index of current element
    int ac = 0; // ac is area of current element

    for (ic = 0; ic < faces.size(); ic++) // Iterate through all current elements (detected faces)

    {
        roi_c.x = faces[ic].x;
        roi_c.y = faces[ic].y;
        roi_c.width = (faces[ic].width);
        roi_c.height = (faces[ic].height);

        ac = roi_c.width * roi_c.height; // Get the area of current element (detected face)

        crop = frame(roi_c);

        resize(crop, res, outputImageSize, 0, 0, INTER_LINEAR); // This will be needed later while saving images
        cvtColor(res, gray, CV_BGR2GRAY); // Convert cropped image to Grayscale

        Point pt1(faces[ic].x, faces[ic].y); // Display detected faces on main window - live stream from camera
        Point pt2((faces[ic].x + faces[ic].height), (faces[ic].y + faces[ic].width));
        rectangle(frame, pt1, pt2, Scalar(0, 255, 0), 2, 8, 0);

        imshow("Face Detection Visualization", frame);//display detected face in rectangel box
        int c = waitKey(1);//wait as less as possible

    }

    imshow("Face Detection Visualization", frame);//also display image in which face is not detected

}
