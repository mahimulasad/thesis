cd 'E:/Eclipse_setting/ThesisTest/data/current/training';

same_dis = fopen('same_person_dis.txt', 'r');
A = fscanf(same_dis, '%d');
fclose(same_dis);

same_eu_dis = fopen('same_person_eu_dis.txt', 'r');
B = fscanf(same_eu_dis, '%d');
fclose(same_eu_dis);

diff_dis = fopen('different_person_dis.txt', 'r');
C = fscanf(diff_dis, '%d');
fclose(diff_dis);

diff_eu_dis = fopen('different_person_eu_dis.txt', 'r');
D = fscanf(diff_eu_dis, '%d');
fclose(diff_eu_dis);

figure

subplot(2,2,[1,2]);
hist(C, 1000);
hold on;
hist(A, 1000);
title('Similar(black)(top) and Different(blue)');

subplot(2,2,3);
hist(A, 100);
title('Similar Face histogram');


subplot(2,2,4);
hist(C, 100);
title('Different Face histogram');
