cd 'E:/Eclipse_setting/ThesisTest/data/current/training';

same_dis = fopen('same_person_dis.txt', 'r');
A = fscanf(same_dis, '%d');
fclose(same_dis);

same_eu_dis = fopen('same_person_eu_dis.txt', 'r');
B = fscanf(same_eu_dis, '%d');
fclose(same_eu_dis);

diff_dis = fopen('different_person_dis.txt', 'r');
C = fscanf(diff_dis, '%d');
fclose(diff_dis);

diff_eu_dis = fopen('different_person_eu_dis.txt', 'r');
D = fscanf(diff_eu_dis, '%d');
fclose(diff_eu_dis);

figure
subplot(2,2,1);
plot(A, B, '.', C, D, 'r.');
title('Similar(blue) and Different(red)(top)');

subplot(2,2,2);
plot(C, D,'r.', A, B, '.');
title('Similar(blue)(top) and Different(red)');

subplot(2,2,3);
plot(A, B,'.');
title('Similar Face');


subplot(2,2,4);
plot(C,D, 'r.');
title('Different Face');
